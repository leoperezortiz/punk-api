package com.model.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Leo Perez Ortíz on 6/2/20.
 */
@Entity
data class Beer(
    var brewers_tips: String = "", // Make sure you have plenty of room in the fermenter. Beers containing wheat can often foam aggressively during fermentation.
    var contributed_by: String = "", // Sam Mason <samjbmason>
    var description: String =
        "", // 2008 Prototype beer, a 4.7% wheat ale with crushed juniper berries and citrus peel.
    var first_brewed: String = "", // 10/2008
    @PrimaryKey
    var id: Int = 0, // 25
    var image_url: String? = "", // https://images.punkapi.com/v2/25.png
    var name: String = "", // Bad Pixie
    var tagline: String = "" // Spiced Wheat Beer.
)