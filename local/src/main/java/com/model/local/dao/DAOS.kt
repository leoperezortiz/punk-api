package com.model.local.dao

import androidx.room.*
import com.model.local.entity.Beer

/**
 * Created by Leo Perez Ortíz on 2019-09-12.
 */
interface BaseDao<T> {

    /**
     * Insert an object in the database.
     *
     * @param obj the object to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(obj: T)

    @Transaction
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(obj: List<T>)

    /**
     * Insert an array of objects in the database.
     *
     * @param obj the objects to be inserted.
     */
    @Insert
    fun insert(vararg obj: T)

    /**
     * Update an object from the database.
     *
     * @param obj the object to be updated
     */
    @Update
    fun update(obj: T)

    @Update
    fun update(obj: List<T>)

    /**
     * Delete an object from the database
     *
     * @param obj the object to be deleted
     */
    @Delete
    fun delete(obj: T)

}

@Dao
interface BeerDao : BaseDao<Beer> {
    @Query("select count(*) from Beer")
    fun getCount(): Int

    @Query("select * from Beer")
    fun getAll(): List<Beer>
}
