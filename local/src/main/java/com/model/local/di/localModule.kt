package com.model.local.di

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.model.local.dao.BeerDao
import com.model.local.entity.Beer
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

/**
 * Created by Leo Perez Ortíz on 2019-07-10.
 */
val localModule = module {
    single { createDb(androidApplication()) }
}

fun createDb(context: Context): PunkDb {
    return Room.databaseBuilder(
        context, PunkDb::class.java,
        "PunkApi"
    ).fallbackToDestructiveMigration().build()
}

@Database(entities = [Beer::class], exportSchema = false, version = 1)
abstract class PunkDb : RoomDatabase() {
    abstract fun beerDao(): BeerDao
}

