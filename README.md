# README #

### What is this repository for? ###

* Aplicacion de muestra que consume los servicios de https://punkapi.com/.
* Se aplico arquitectura MVVM+patron repository (incluyendo modulos de gestion de cache local, remota, e injeccion de dependecia KOIN)
* Para el trabajo con listas (recyclerview) se uso la librería groupie para agilizar el manejo de los adaptadores.
* Para la inyección de dependecias se uso koin, que posee una curva de aprendizaje muy baja y se integra perfectamente entre los módulos del proyecto.
* Para el manejo de la caché local de los datos se uso room como gestor de base de datos.
* En el proyecto se incluyen clases y utilidades genéricas para el trabajo con peticiones REST así como Daos genéricos para el trabajo con room, entre otras clases que se puedan encontrar.
* Puede que existan dependencias que no se usen pues suelo partir de un esqueleto global para los proyectos.