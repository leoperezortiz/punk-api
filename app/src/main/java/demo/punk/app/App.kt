package demo.punk.app

import androidx.multidex.MultiDexApplication
import com.blankj.utilcode.util.Utils
import com.dev.leo.di.appModule
import com.model.local.di.localModule
import com.module.remote.di.remoteModule
import com.module.repository.di.repositoryModule
import core.di.coreModule
import net.danlew.android.joda.JodaTimeAndroid
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Created by Leo Perez Ortíz on 5/13/20.
 */
class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(
                arrayListOf(
                    remoteModule("https://api.punkapi.com/v2/", this@App), localModule,
                    repositoryModule, appModule, coreModule
                )
            )
        }
        Utils.init(this)
        JodaTimeAndroid.init(this)
    }
}