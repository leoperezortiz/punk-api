package demo.punk.app.presenter.fragment.ui.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.model.local.entity.Beer
import com.module.remote.api.Api
import com.module.remote.responses.BeersResponse
import com.module.remote.util.ApiCallState
import com.star_zero.pagingretrofitsample.paging.BeerDataSourceFactory
import demo.punk.app.model.adapterItems.BeerItem

class SearchViewModel(val api: Api) : ViewModel() {

    companion object {
        private const val PAGE_SIZE = 50
    }

    var query = ""
    lateinit var beersPagedList: LiveData<PagedList<BeerItem>>
    lateinit var networkState: LiveData<ApiCallState<BeerItem>>
    lateinit var dataSourceFactory: BeerDataSourceFactory<BeerItem>

    fun initPaging() {
        dataSourceFactory = BeerDataSourceFactory(
            api,
            viewModelScope,
            query,
            ::convertToBeerItem
        )
        val config = PagedList.Config.Builder()
            .setInitialLoadSizeHint(PAGE_SIZE)
            .setPageSize(PAGE_SIZE)
            .build()

        beersPagedList = LivePagedListBuilder(dataSourceFactory, config).build()
        networkState = dataSourceFactory.source.networkState

    }

    private fun convertToBeerItem(response: BeersResponse) = BeerItem(
        Beer(
            response.brewers_tips,
            response.contributed_by,
            response.description,
            response.first_brewed,
            response.id,
            response.image_url,
            response.name,
            response.tagline
        )
    )
}