package com.porter.client.presenter.acitivities

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.afollestad.materialdialogs.MaterialDialog
import com.module.remote.util.ApiCallState
import com.module.remote.util.PreferenceManager
import demo.punk.app.R
import org.jetbrains.anko.intentFor
import org.koin.android.ext.android.inject


/**
 * Created by Leo Perez Ortíz on 4/7/18.
 */
open class BaseActivity : AppCompatActivity() {

    lateinit var dialog: MaterialDialog
    val pref: PreferenceManager by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialog = MaterialDialog.Builder(this).build()
        initViews()
        initObservers()
        loadParametrization()
    }

    fun showLoading(content: Int) {
        dialog.let {
            if (it.isShowing)
                it.dismiss()
        }
        dialog = MaterialDialog.Builder(this)
            .cancelable(false)
            .progress(true, 30)
            .content(content)
            .widgetColor(ContextCompat.getColor(this, R.color.colorPrimary))
            .build()
        dialog.show()
    }

    fun showErrorDialog(
        title: String = "Aviso", text: String, actionOk: (() -> Unit)? = null,
        actionNo: (() -> Unit)? = null
    ) {
        dialog.let {
            if (it.isShowing)
                it.dismiss()
        }
        dialog = MaterialDialog.Builder(this)
            .cancelable(false)
            .title(title)
            .content(text)
            .positiveText("Ok")
            .onPositive { _, _ ->
                actionOk?.let {
                    dialog.dismiss()
                    actionOk?.invoke()
                }
            }
            .onNegative { _, _ ->
                actionNo?.let {
                    actionNo.invoke()
                    dialog.dismiss()
                }
            }
            .build()
        dialog.show()
    }

    open fun initViews() {}

    open fun initObservers() {}

    open fun loadParametrization() {}

    fun <T> handleNetworkState(
        showLoading: Boolean = true, state: LiveData<ApiCallState<T>>,
        render: ((data: T) -> Unit)? = null,
        error: ((error: String) -> Unit)? = null,
        loading: (() -> Unit)? = null,
        dismiss: (() -> Unit)? = null
    ) {
        state.observe(this, Observer {
            when (it.status) {
                ApiCallState.Status.LOADING -> {
                    if (loading == null && showLoading)
                        showLoading(R.string.loading)
                    else
                        loading?.let { it1 -> it1() }
                }
                ApiCallState.Status.SUCCESS -> {
                    dialog.dismiss()
                    render?.apply {
                        if (it.data != null)
                            this(it.data!!)
                    }
                }
                else -> {
                    if (it.apiError != null && error == null) {// api error and no custom error handling
                        showErrorDialog(text = it.apiError!!)
                    } else if (it.apiError != null && error != null) {// api error and custom error handling
                        error.invoke(it.apiError!!)
                    } else if (error == null) { //no custom error handling
                        dialog.dismiss()
                    } else {// custom error handling
                        error?.invoke(it.error!!.localizedMessage)
                        dialog.dismiss()
                    }
                }
            }
        })
    }

    inline fun <reified T : Any> openActivity(vararg params: Pair<String, Any?>) {
        startActivity(intentFor<T>(*params))
        overridePendingTransition(R.anim.activity_in_right, R.anim.slide_out_right)
    }

    inline fun <reified T : AppCompatActivity> openActivityAndCloseOthers(vararg params: Pair<String, Any?>) {
        startActivity(intentFor<T>(*params))
        overridePendingTransition(R.anim.activity_in_right, R.anim.slide_out_right)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_in_left, R.anim.activity_out_left)
        finish()
    }

}
