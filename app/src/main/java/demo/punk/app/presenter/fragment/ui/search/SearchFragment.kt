package demo.punk.app.presenter.fragment.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.PagedList
import androidx.recyclerview.widget.DividerItemDecoration
import com.porter.client.presenter.acitivities.BaseFragment
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import demo.punk.app.R
import demo.punk.app.model.adapterItems.BeerItem
import demo.punk.app.pagging.PagedListGroup
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchFragment : BaseFragment() {

    private val searchViewModel: SearchViewModel by viewModel()

    private val pagedListGroup = PagedListGroup<BeerItem>()
    private val queryChannel = ConflatedBroadcastChannel<String>()

    private val pagesObserver = Observer<PagedList<BeerItem>> { t -> pagedListGroup.submitList(t) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        return root
    }

    override fun initViews() {
        queryChannel.asFlow().debounce(500L)
            .onEach {
                searchViewModel.query = it
                initObservers()
            }
            .launchIn(lifecycleScope)

        et_search.addTextChangedListener {
            it?.let {
                if (it.isNotEmpty()) {
                    queryChannel.offer(it.toString().replace(" ","_"))
                }
            }
        }

        rv_search.apply {
            adapter = GroupAdapter<GroupieViewHolder>().apply {
                add(pagedListGroup)
            }
            addItemDecoration(DividerItemDecoration(requireContext(), LinearLayout.VERTICAL))
        }

    }

    override fun initObservers() {
        searchViewModel.initPaging()
        handleNetworkState(state = searchViewModel.networkState,
            loading = {

            },
            render = {

            })
        searchViewModel.beersPagedList.observe(this, pagesObserver)
    }
}
