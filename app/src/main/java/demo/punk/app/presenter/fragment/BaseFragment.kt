package com.porter.client.presenter.acitivities

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.afollestad.materialdialogs.MaterialDialog
import com.module.remote.util.ApiCallState
import com.module.remote.util.PreferenceManager
import demo.punk.app.R
import org.jetbrains.anko.intentFor
import org.koin.android.ext.android.inject

/**
 * Created by Leo Perez Ortíz on 4/7/18.
 */
open class BaseFragment : Fragment() {

    lateinit var dialog: MaterialDialog
    val pref: PreferenceManager by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialog = MaterialDialog.Builder(requireContext()).build()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initObservers()
        initViews()
    }

    fun showLoading(content: Int) {
        dialog?.let {
            if (it.isShowing)
                it.dismiss()
        }
        dialog = MaterialDialog.Builder(requireContext())
            .cancelable(false)
            .progress(true, 30)
            .content(content)
            .build()
        dialog.show()
    }

    fun showErrorDialog(
        title: String = "Aviso", text: String, actionOk: (() -> Unit)? = null,
        actionNo: (() -> Unit)? = null
    ) {
        dialog.let {
            if (it.isShowing)
                it.dismiss()
        }
        dialog = MaterialDialog.Builder(requireContext())
            .cancelable(false)
            .content(text)
            .positiveText("Ok")
            .build()
        dialog.show()
    }

    open fun initViews() {}

    open fun initObservers() {}

    inline fun <reified T : Any> openActivity(vararg params: Pair<String, Any?>) {
        startActivity(requireContext().intentFor<T>(*params))
        activity?.overridePendingTransition(R.anim.activity_in_right, R.anim.slide_out_right);
    }

    fun <T> handleNetworkState(
        showLoading: Boolean = true, state: LiveData<ApiCallState<T>>,
        render: ((data: T?) -> Unit)? = null,
        error: ((error: String) -> Unit)? = null,
        loading: (() -> Unit)? = null,
        dismiss: (() -> Unit)? = null
    ) {
        state.observe(this, Observer {
            when (it.status) {
                ApiCallState.Status.LOADING -> {
                    if (loading == null && showLoading)
                        showLoading(R.string.loading)
                    else
                        loading?.let { it1 -> it1() }
                }
                ApiCallState.Status.SUCCESS -> {
                    dialog.dismiss()
                    render?.apply {
                        this(it.data)
                    }
                }
                else -> {
                    if (it.apiError != null) {
                        showErrorDialog(text = it.apiError!!, actionOk = dismiss)
                    } else if (error == null)
                        showErrorDialog("Error", (it).error!!.localizedMessage, dismiss)
                    else
                        error?.invoke(it.error!!.localizedMessage)
                }
            }
        })
    }

}