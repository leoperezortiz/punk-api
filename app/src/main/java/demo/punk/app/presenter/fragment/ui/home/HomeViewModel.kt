package demo.punk.app.presenter.fragment.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.model.local.entity.Beer
import com.module.remote.responses.BeersResponse
import com.module.remote.util.ApiCallObserver
import com.module.repository.repository.BeerRepository
import kotlinx.coroutines.launch

class HomeViewModel(val repository: BeerRepository) : ViewModel() {

    val beersObserver: ApiCallObserver<List<Beer>> by lazy {
        return@lazy ApiCallObserver<List<Beer>>()
    }

    fun beers() {
        viewModelScope.launch {
            beersObserver.build(this, repository.getBeers())
        }
    }
}