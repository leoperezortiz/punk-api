package demo.punk.app.presenter.fragment.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import com.module.remote.hide
import com.module.remote.visible
import com.porter.client.presenter.acitivities.BaseFragment
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import demo.punk.app.R
import demo.punk.app.model.adapterItems.BeerItem
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment() {

    val model: HomeViewModel by viewModel()
    val adapterBeers = GroupAdapter<GroupieViewHolder>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rv_beers.apply {
            adapter = adapterBeers
            addItemDecoration(DividerItemDecoration(requireContext(), LinearLayout.VERTICAL))
        }

        model.beers()
    }

    override fun initObservers() {
        handleNetworkState(
            loading = { progress_circular.visible() },
            state = model.beersObserver.observable,
            render = { items ->
                rv_beers.apply {
                    adapterBeers.addAll(items!!.map { return@map BeerItem(it) })
                    progress_circular.hide()
                }
            }
        )
    }
}
