package com.dev.leo.di

import demo.punk.app.presenter.fragment.ui.home.HomeViewModel
import demo.punk.app.presenter.fragment.ui.search.SearchViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


/**
 * Created by Leo Perez Ortíz on 4/2/18.
 */

var appModule = module {

    viewModel { HomeViewModel(get()) }
    viewModel { SearchViewModel(get()) }
}
