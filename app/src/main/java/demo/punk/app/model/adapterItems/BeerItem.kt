package demo.punk.app.model.adapterItems;

import coil.api.load
import com.model.local.entity.Beer
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import demo.punk.app.R
import kotlinx.android.synthetic.main.beer_item.view.*

/**
 * Created by Leo Perez Ortíz on 5/12/20.
 */
class BeerItem(val beer: Beer) : Item() {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.apply {
            beer_title.text = beer.name
            beer_sub_title.text = beer.tagline
            imv_beer.load(beer.image_url)
        }
    }

    override fun getLayout(): Int {
        return R.layout.beer_item
    }

    override fun isSameAs(other: com.xwray.groupie.Item<*>?): Boolean {
        return beer.id == (other as BeerItem).beer.id
    }
}