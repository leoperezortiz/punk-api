package com.star_zero.pagingretrofitsample.paging

import androidx.paging.DataSource
import com.module.remote.api.Api
import com.module.remote.responses.BeersResponse
import com.xwray.groupie.Item
import kotlinx.coroutines.CoroutineScope

class BeerDataSourceFactory<T : Item<*>>(
    api: Api, scope: CoroutineScope,
    query: String,
    convert: (BeersResponse) -> T
) : DataSource.Factory<Int, T>() {

    val source = BeerKeyedRepoDataSource(api, scope, query, convert)

    override fun create(): DataSource<Int, T> {
        return source
    }
}
