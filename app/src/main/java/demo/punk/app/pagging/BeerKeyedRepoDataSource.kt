package com.star_zero.pagingretrofitsample.paging

import android.util.Log
import androidx.annotation.MainThread
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.module.remote.api.Api
import com.module.remote.responses.BeersResponse
import com.module.remote.util.ApiCallState
import com.module.remote.util.CallUtil
import com.xwray.groupie.Item
import kotlinx.coroutines.*
import java.io.IOException

class BeerKeyedRepoDataSource<T : Item<*>>(
    private val api: Api,
    val scope: CoroutineScope,
    val query: String,
    val convert: (BeersResponse) -> T
) : PageKeyedDataSource<Int, T>() {

    companion object {
        private const val TAG = "PageKeyedRepoDataSource"
    }

    val networkState = MutableLiveData<ApiCallState<T>>()

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, T>) {
        // not used
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, T>) {
        scope.launch(Dispatchers.IO) {
            callAPI(params.key, params.requestedLoadSize) { repos, next ->
                callback.onResult(repos.map(convert), next)
            }
        }
    }

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, T>
    ) {
        scope.launch(Dispatchers.IO) {
            callAPI(1, params.requestedLoadSize) { repos, next ->
                callback.onResult(repos.map(convert), null, next)
            }
        }
    }

    private suspend fun callAPI(
        page: Int,
        perPage: Int,
        callback: (repos: List<BeersResponse>, next: Int?) -> Unit
    ) {
        Log.d(TAG, "page: $page, perPage: $perPage")

        setValue(ApiCallState(ApiCallState.Status.LOADING))

        try {
            if (query.isNullOrEmpty())
                return callback(listOf(), 1)
            val response = api.searchBeers(query, page).await()
            callback(response, page + 1)
            setValue(ApiCallState.success(null))
        } catch (e: IOException) {
            Log.w(TAG, e)
        }
    }


    @MainThread
    private fun setValue(newValue: ApiCallState<T>) {
        Log.d(CallUtil::class.java.name, "Resource: " + newValue)
        networkState.postValue(newValue)
    }

}