/**
 * Created by Leo Perez Ortíz on 20/8/18.
 */

object Version {
    const val kot_version = "1.3.41"
    const val kot_corroutine = "1.0.0"
    const val suppotLib = "28.0.0"
    const val constrintJet = "2.0.0-alpha2"
    const val playServices = "17.0.0"
    const val playServicesAuth = "17.0.0"
    const val fireBase = "17.0.1"
    const val fireBaseAuth = "17.0.0"
    const val fireBaseMessaging = "19.0.1"
    const val fireBaseDataBase = "18.0.1"
    const val fireBaseAnalitycs = "17.2.1"
    const val joda = "2.9.9.3"
    const val koin = "2.1.5"
    const val anko = "0.10.5"
    const val pagging = "1.0.1"
    const val retrofit = "2.3.0"
    const val gson = "2.8.2"
    const val retrofitCorroutine = "0.9.2"
    const val retrofitCOnverter = "2.0.0"
    const val retrofitInterceptro = "3.8.0"
    const val retrofitAnnotation = "4.0-b33"
    const val room = "2.2.0-rc01"
    const val utilCode = "1.25.8"
    const val realm = "5.8.0"
    const val gradleBuild = "3.4.2"
    const val playServicesPlugin = "4.3.0"
    const val eventBus = "3.1.1"
    const val jetLifecycle = "2.2.0-alpha01"
}

object Depend {

    const val multidex = "com.android.support:multidex:1.0.2"
    const val gradleBuild = "com.android.tools.build:gradle:${Version.gradleBuild}"
    const val kotlinVersion = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Version.kot_version}"
    const val kotlinAndroidExtention= "org.jetbrains.kotlin:kotlin-android-extensions:${Version.kot_version}"
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Version.kot_version}"
    const val kotlinCorroutine = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Version.kot_corroutine}"

    const val appCompact = "androidx.appcompat:appcompat:1.0.2"
    const val design = "com.google.android.material:material:1.2.0-alpha06"
    const val cardView = "com.android.support:cardview-v7:${Version.suppotLib}"
    const val v4 = "androidx.legacy:legacy-support-v4:1.0.0"
    const val v13 = "com.android.support:support-v13:${Version.suppotLib}"
    const val customTabs = "com.android.support:customtabs:"
    const val exifInterface = "com.android.support:exifinterface:${Version.suppotLib}"
    const val dynamicAnimation = "com.android.support:support-dynamic-animation:${Version.suppotLib}"
    const val recycler = "androidx.recyclerview:recyclerview:1.0.0-alpha1"
    const val constraint = "androidx.constraintlayout:constraintlayout:${Version.constrintJet}"

    const val playServicesPlugin = "com.google.gms:google-services:${Version.playServicesPlugin}"
    const val playLocation = "com.google.android.gms:play-services-location:${Version.playServices}"
    const val playPaces = "com.google.android.gms:play-services-places:${Version.playServices}"
    const val playAuth = "com.google.android.gms:play-services-auth:${Version.playServicesAuth}"
    const val playAds = "com.google.android.gms:play-services-ads:${Version.playServices}"
    const val playMaps = "com.google.android.gms:play-services-maps:${Version.playServices}"

    const val fireBaseDatabase = "com.google.firebase:firebase-database:${Version.fireBaseDataBase}"
    const val fireBaseAnalitycs = "com.google.firebase:firebase-analytics:${Version.fireBaseAnalitycs}"
    const val fireBaseMessaging = "com.google.firebase:firebase-messaging:${Version.fireBaseMessaging}"
    const val fireBaseAuth = "com.google.firebase:firebase-auth:${Version.fireBaseAuth}"
    const val fireBaseCore = "com.google.firebase:firebase-core:${Version.fireBase}"

    const val joda = "net.danlew:android.joda:${Version.joda}"
    const val koinCore = "org.koin:koin-android:${Version.koin}"
    const val koinScope = "org.koin:koin-androidx-scope:${Version.koin}"
    const val koinArch = "org.koin:koin-androidx-viewmodel:${Version.koin}"

    const val room = "androidx.room:room-runtime:${Version.room}"
    const val roomProccesor = "androidx.room:room-compiler:${Version.room}"
    const val roomKtx = "androidx.room:room-ktx:${Version.room}"

    const val anko = "org.jetbrains.anko:anko-commons:${Version.anko}"
    const val coil = "io.coil-kt:coil:0.10.1"

    const val lifecycle = "androidx.lifecycle:lifecycle-extensions:${Version.jetLifecycle}"
    const val lifecycleViewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Version.jetLifecycle}"
    const val lifecycleViewLivedata = "androidx.lifecycle:lifecycle-livedata-ktx:${Version.jetLifecycle}"
    const val workManager = "androidx.work:work-runtime-ktx${Version.jetLifecycle}"

    const val groupie = "com.xwray:groupie:2.6.0"
    const val groupieKtx = "com.xwray:groupie-kotlin-android-extensions:2.6.0"

    const val lifecycleRuntime = "androidx.lifecycle:lifecycle-runtime-ktx:${Version.jetLifecycle}"
    const val lifecycleCompiler = "androidx.lifecycle:lifecycle-compiler:${Version.jetLifecycle}"

    const val paggin = "android.arch.lifecycle:compiler:${Version.pagging}"

    const val retrofit = "com.squareup.retrofit2:retrofit:${Version.retrofit}"
    const val retrofitGson = "com.google.code.gson:gson:${Version.gson}"
    const val retrofitConverter = "com.squareup.retrofit2:converter-gson:${Version.retrofitCOnverter}"
    const val retrofiCorroutine = "com.jakewharton.retrofit:retrofit2-kotlin-coroutines-adapter:${Version.retrofitCorroutine}"
    const val retrofitLoginInterceptor = "com.squareup.okhttp3:logging-interceptor:${Version.retrofitInterceptro}"
    const val retrofitAnnotations = "org.glassfish.main:javax.annotation:${Version.retrofitAnnotation}"

    const val util = "com.blankj:utilcodex:${Version.utilCode}"
    const val picasso = "com.squareup.picasso:picasso:2.5.2"
    const val picassoTransfrom = "jp.wasabeef:picasso-transformations:2.2.1"

    const val glide = "com.github.bumptech.glide:glide:4.9.0"
    const val glideProcessor = "com.github.bumptech.glide:compiler:4.9.0"
    const val glideTransformation="jp.wasabeef:glide-transformations:4.1.0"


    //DatePicker
    const val datePicker = "com.wdullaer:materialdatetimepicker:3.6.1"
    const val circleImageView = "de.hdodenhof:circleimageview:2.2.0"
    //debugear sqliteDB
    const val debugSqLite = "com.amitshekhar.android:debug-db:1.0.4"
    //ImagePicker
    const val imagePicker = "com.github.jkwiecien:EasyImage:1.3.1"
    //permissionlib
    const val permission = "com.github.fondesa:kpermissions:1.0.0"
    const val materialDialogs = "com.afollestad.material-dialogs:core:0.9.6.0"
    const val flexBoxLayout = "com.google.android:flexbox:1.1.0"
    const val materialRangeBar = "com.appyvet:materialrangebar:1.4.3"
    const val facebook = "com.facebook.android:facebook-android-sdk:[4,5)"
    const val pageIndicator = "com.romandanylyk:pageindicatorview:1.0.1@aar"
    const val eventBuss = "org.greenrobot:eventbus:${Version.eventBus}"
    //Location Library
    const val myLocation = "io.nlopez.smartlocation:library:3.3.3"
    const val compressImage = "id.zelory:compressor:2.1.0"
    const val lottie = "com.airbnb.android:lottie:2.8.0"

}