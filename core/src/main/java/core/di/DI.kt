package core.di

import com.module.remote.util.PreferenceManager
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module


/**
 * Created by Leo Perez Ortíz on 2019-08-12.
 */
val coreModule = module {
    single { PreferenceManager(androidApplication()) }
}
