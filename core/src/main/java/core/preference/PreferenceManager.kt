package com.module.remote.util

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by Leo Perez Ortíz on 4/2/18.
 */

const val SESSION_ID = "si"

class PreferenceManager {

    var preference: SharedPreferences

    constructor(context: Context) {
        preference = context.getSharedPreferences("PREF", Context.MODE_PRIVATE)
    }

    fun clear() {
        preference.edit().clear().apply()
    }
}