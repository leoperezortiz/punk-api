package com.module.remote

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.biduz.core.BuildConfig
import com.blankj.utilcode.util.ToastUtils
import org.jetbrains.anko.forEachChild


/**
 * Created by Leo Perez Ortíz on 6/1/18.
 */

fun ViewGroup.inflate(layoutRes: Int): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, false)
}


inline fun Context.launchUrl(url: String) {
    var intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
    startActivity(intent)
}

fun Context.setToolbarFont(toolBar: Toolbar, font: String) {
    toolBar.forEachChild {
        if (it is TextView) {
            it.typeface = Typeface.createFromAsset(assets, font)
        }
    }
}

fun Context.toast(msg: String, force: Boolean = false) {
    if (BuildConfig.DEBUG || force)
        ToastUtils.showShort(msg)
}
fun View.hide() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun View.visible() {
    visibility = View.VISIBLE
}