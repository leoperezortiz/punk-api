package core.config

import android.os.Environment
import com.blankj.utilcode.util.AppUtils
import java.io.File

object env {

    var url = "https://testing.biduzz.com" //local
    var url_chat = "https://testing-chatweb.biduzz.com/"
    var urlInviteCode = "http://testing-chatweb.biduzz.com/invitation-user/"
    var ws = "https://testing.biduzz.com/app/"    //local
    var wsCodePNG = "https://devcdn.biduzz.com/user/"
    val hostChat = "testing.biduzz.com"
    val hostUsers = "testing.biduzz.com"
    val chatPort = 5222
    val urlImagesChats = "http://devcdn.biduzz.com/chats/"
    val stripe = "pk_test_NT9gbq3ptpYLMBx2QUfuY2TS"
    val baseParamsUrl="http://testing.biduzz.com/app/customizer"
//
    val fileDir = Environment.getExternalStorageDirectory().absolutePath.plus(File.separator)
            .plus(AppUtils.getAppName())
//
//    var url = "https://biduzz.com" //local
//    var url_chat = "https://chatweb.biduzz.com/"
//    var urlInviteCode = "http://chatweb.biduzz.com/invitation-user/"
//    var ws = "https://biduzz.com/app/"
//    var wsCodePNG = "https://cdn.biduzz.com/user/"
//    val hostChat = "xmpp.biduzz.com"
//    val chatPort = 5222
//    val urlImagesChats = "http://cdn.biduzz.com/chats/"
//    val stripe = "pk_live_q2iuJ6jneynAroYfpnLyWCTf"
//
//    var url_server = "https://d8ye4q86842is.cloudfront.net"
}
