package com.module.repository.repository

import androidx.lifecycle.LiveData
import com.model.local.di.PunkDb
import com.model.local.entity.Beer
import com.module.remote.api.Api
import com.module.remote.responses.BeersResponse
import com.module.remote.util.ApiCallState
import com.module.remote.util.CallUtil
import com.module.remote.util.PreferenceManager
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Created by Leo Perez Ortíz on 5/16/20.
 */
class BeerRepository(val api: Api, val db: PunkDb) {

    suspend fun getBeers(): LiveData<ApiCallState<List<Beer>>> {
        return object : CallUtil<List<Beer>, ArrayList<BeersResponse>>() {
            override suspend fun processResponse(response: ArrayList<BeersResponse>): List<Beer> {
                return response.map {
                    return@map Beer(
                        it.brewers_tips, it.contributed_by, it.description, it.first_brewed, it.id,
                        it.image_url, it.name, it.tagline
                    )
                }
            }

            override suspend fun saveCallResults(items: List<Beer>) {
                withContext(Dispatchers.IO) { db.beerDao().insert(items) }
            }

            override fun shouldFetch(data: List<Beer>?): Boolean {
                return true
            }

            override suspend fun useCache(): Boolean {
                return withContext(Dispatchers.IO) { db.beerDao().getCount() > 0 }
            }

            override suspend fun loadFromDb(): List<Beer> {
                return withContext(Dispatchers.IO) { db.beerDao().getAll() }
            }

            override suspend fun createCallAsync(): Deferred<ArrayList<BeersResponse>> {
                return api.getBeers()
            }

        }.build().asLiveData()
    }
}