package com.module.repository.di

import com.module.repository.repository.*
import org.koin.dsl.module

/**
 * Created by Leo Perez Ortíz on 2019-07-09.
 */
val repositoryModule = module {

    single { BeerRepository(get(), get()) }
}
