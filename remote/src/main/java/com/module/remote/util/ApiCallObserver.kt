package com.module.remote.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * Created by Leo on 2019-08-16.
 */
class ApiCallObserver<T>() {
    private val _mediatorPolicy = MediatorLiveData<ApiCallState<T>>()
    val observable: LiveData<ApiCallState<T>> get() = _mediatorPolicy
    private var policySource: LiveData<ApiCallState<T>> = MutableLiveData()

    fun build(scope: CoroutineScope, action: LiveData<ApiCallState<T>>) {
        scope.launch(Dispatchers.Main) {
            withContext(Dispatchers.IO) { policySource = action }
            _mediatorPolicy.removeSource(policySource)
            _mediatorPolicy.addSource(policySource) {
                _mediatorPolicy.value = it
            }
        }
    }
}