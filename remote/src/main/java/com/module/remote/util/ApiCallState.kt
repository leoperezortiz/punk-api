package com.module.remote.util


/**
 * Created by Leo on 2019-07-09.
 */

data class ApiCallState<out T>(
    val status: Status,
    val data: T? = null,
    val apiError: String? = null,
    val error: Throwable? = null
) {
    companion object {
        fun <T> success(data: T?): ApiCallState<T> {
            return ApiCallState(
                Status.SUCCESS,
                data,
                null
                , null
            )
        }

        fun <T> error(error: Throwable, data: T?): ApiCallState<T> {
            return ApiCallState(
                Status.ERROR,
                data,
                null,
                error
            )
        }

        fun <T> apiError(error: String, data: T?): ApiCallState<T> {
            return ApiCallState(
                Status.ERROR,
                data,
                error,
                null
            )
        }

        fun <T> loading(data: T? = null): ApiCallState<T> {
            return ApiCallState(
                Status.LOADING,
                data,
                null,
                null
            )
        }
    }

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING
    }
}