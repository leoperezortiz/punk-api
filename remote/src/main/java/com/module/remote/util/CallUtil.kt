package com.module.remote.util

import android.util.Log
import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import kotlin.coroutines.coroutineContext

/**
 * Created by Leo on 2019-07-09.
 */

abstract class CallUtil<ResultType, RequestType> {

    private val result = MutableLiveData<ApiCallState<ResultType>>()
    private val supervisorJob = SupervisorJob()

    suspend fun build(): CallUtil<ResultType, RequestType> {
        withContext(Dispatchers.Main) {
            result.value =
                ApiCallState.loading(null)
        }
        CoroutineScope(coroutineContext).launch(supervisorJob) {
            if (useCache()) {
                val dbResult = loadFromDb()
                if (dbResult != null) {
                    Log.d(CallUtil::class.java.name, "Return data from local database")
                    setValue(ApiCallState.success(dbResult))
                    if (shouldFetch(dbResult))
                        fetchFromNetwork(false)
                } else
                    fetchFromNetwork()
            } else
                fetchFromNetwork()
        }
        return this
    }

    fun asLiveData() = result as LiveData<ApiCallState<ResultType>>

    // ---

    private suspend fun fetchFromNetwork(notify: Boolean = true) {
        try {
            Log.d(CallUtil::class.java.name, "Fetch data from network")
            if (notify)
                setValue(ApiCallState.loading())
            val apiResponse = createCallAsync().await()
            Log.e(CallUtil::class.java.name, "Data fetched from network")
//            if (!(apiResponse as BaseResponse<*>).message.isNullOrEmpty()) {
//                if (!useCache() || loadFromDb() != null)
//                    setValue(ApiCallState.apiError((apiResponse as BaseResponse<*>).message, null))
//                return
//            }
            val response = processResponse(apiResponse)
            saveCallResults(response)
            if (notify && useCache()) {
                setValue(ApiCallState.success(loadFromDb()))
            } else
                setValue(ApiCallState.success(response))
        } catch (e: Exception) {
//            if (notify && useCache())
//                setValue(ApiCallState.error(e, loadFromDb()))
//            else
            if (!useCache() || loadFromDb() != null)
                setValue(ApiCallState.error(e, null))
            Log.e("CallUtil", "An error happened: $e")
            e.printStackTrace()
        }
    }

    @MainThread
    private fun setValue(newValue: ApiCallState<ResultType>) {
        Log.d(CallUtil::class.java.name, "Resource: " + newValue)
        if (result.value != newValue) result.postValue(newValue)
    }

    @WorkerThread
    protected abstract suspend fun processResponse(response: RequestType): ResultType

    @WorkerThread
    protected abstract suspend fun saveCallResults(items: ResultType)

    @WorkerThread
    protected abstract fun shouldFetch(data: ResultType?): Boolean

    @MainThread
    protected abstract suspend fun useCache(): Boolean

    @MainThread
    protected abstract suspend fun loadFromDb(): ResultType?

    @MainThread
    protected abstract suspend fun createCallAsync(): Deferred<RequestType>
}