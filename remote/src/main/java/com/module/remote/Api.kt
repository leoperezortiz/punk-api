package com.module.remote.api

import com.module.remote.responses.BeersResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query


/**
 * Created by Leo Perez Ortíz on 4/2/18.
 */
interface Api {

    @GET("beers?per_page=80")
    fun getBeers(): Deferred<ArrayList<BeersResponse>>

    @GET("beers")
    fun searchBeers(
        @Query("beer_name") nameFilter: String,
        @Query("page") page: Int,
        @Query("per_page") pageSize: Int = 80
    ): Deferred<List<BeersResponse>>
}