package com.module.remote.responses

/**
 * Created by Leo Perez Ortíz on 6/2/20.
 */
data class BeersResponse(
    var boil_volume: BoilVolume = BoilVolume(),
    var brewers_tips: String = "", // Make sure you have plenty of room in the fermenter. Beers containing wheat can often foam aggressively during fermentation.
    var contributed_by: String = "", // Sam Mason <samjbmason>
    var description: String = "", // 2008 Prototype beer, a 4.7% wheat ale with crushed juniper berries and citrus peel.
    var first_brewed: String = "", // 10/2008
    var food_pairing: List<String> = listOf(),
    var id: Int = 0, // 25
    var image_url: String? = "", // https://images.punkapi.com/v2/25.png
    var ingredients: Ingredients = Ingredients(),
    var method: Method = Method(),
    var name: String = "", // Bad Pixie
    var tagline: String = "", // Spiced Wheat Beer.
    var volume: Volume = Volume()
)

data class Ingredients(
    var hops: List<Hop> = listOf(),
    var malt: List<Malt> = listOf(),
    var yeast: String = "" // Wyeast 1056 - American Ale™
)

data class Hop(
    var add: String = "", // end
    var amount: AmountX = AmountX(),
    var attribute: String = "", // flavour
    var name: String = "" // Sorachi Ace
)

data class Amount(
    var unit: String = "", // grams
    var value: Double = 0.0 // 16.25
)

data class Malt(
    var amount: Amount = Amount(),
    var name: String = "" // Extra Pale
)

data class AmountX(
    var unit: String = "", // kilograms
    var value: Double = 0.0 // 2.06
)

data class Volume(
    var unit: String = "", // litres
    var value: Int = 0 // 20
)

data class Method(
    var fermentation: Fermentation = Fermentation(),
    var mash_temp: List<MashTemp> = listOf(),
    var twist: String = "" // Crushed juniper berries: 12.5g, Lemon peel: 18.8g
)

data class Fermentation(
    var temp: TempX = TempX()
)

data class Temp(
    var unit: String = "", // celsius
    var value: Int = 0 // 19
)

data class MashTemp(
    var duration: Int = 0, // 75
    var temp: Temp = Temp()
)

data class TempX(
    var unit: String = "", // celsius
    var value: Int = 0 // 67
)

data class BoilVolume(
    var unit: String = "", // litres
    var value: Int = 0 // 25
)